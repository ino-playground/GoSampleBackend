# How to avoid deadlock in DB transaction? Queries order matter!

## Abst

- デッドロックの可能性その 2
  - from_account と to_account が逆の TX が同時に発生した場合

## Report

- ロックの解除は基本的には COMMIT もしくは ROLLBACK のタイミング

# Deeply understand Isolation levels and Read phenomena in MySQL & PostgreSQL

## Abst

- データベースのトランザクション使用時は、適切な **isolation level** を設定する必要がある
  - データベースによって若干仕様が異なるので注意しなければならない
- トランザクション分離レベルの概要
  - 最高レベル: トランザクションの相互干渉が一切生じない
  - それ以下: 同時実行されているトランザクションから干渉を受ける = `read phenomenon` (リード現象) が発生
- `read phenomenon`
  - `dirty read`
    - あるトランザクションが、**まだコミットされていない**他の同時実行のトランザクションが書き込んだデータを読み込むこと。
    - コミットされるのか、ロールバックされるのかが未確定なので、誤ったデータを使う可能性が生じる。
  - `non-repeatable read`
    - あるトランザクションが複数回読み込みを行い、その間に他のトランザクションが**対象レコードの更新**をコミットすること。
    - 1 回目に読み込んだデータと 2 回目に読み込んだデータが異なる可能性が生じる。
  - `phantom read`
    - あるトランザクションが複数行の読み込みを複数回行い、その間に他のトランザクションが**新規レコードの追加/削除**をコミットすること。
    - 1 回目に読み込んだデータと 2 回目に読み込んだデータが異なる(レコード数の増減等)可能性が生じる。
  - `serialization anomaly` (直列化異常)
    - 同時にコミットされたトランザクション群を、互いにオーバーラップしないよう任意の順序で順次実行しようとしても、その結果が得られないこと。
    - ref) [いろんな Anomaly - Qiita](https://qiita.com/kumagi/items/5ef5e404546736ebac49#dirty-read-anomaly)
- `isolation level`
  - `read uncommitted` (lowest)
    - この分離レベルのトランザクションでは、他のコミットされていないトランザクションによって書き込まれたデータを見ることができる
  - `read committed`
    - この分離レベルのトランザクションでは、他のトランザクションによってコミットされたデータのみ見ることができる
  - `repeatable read`
    - この分離レベルのトランザクションでは、同一の SELECT のクエリは何度問い合わせても常に同じ結果を返す
  - `serializable` (highest)
    - 同時実行しても、1 つずつ実行した場合と同じ結果を得られることが保証される
- MySQL 8
  - デフォの分離レベルは `REPEATABLE-READ` (by `> select @@transaction_isolation;`)
  - `REPEATABLE-READ` では 他のトランザクションで UPDATE が行われても SELECT の同一性は保たれるが、注目しているトランザクションで UPDATE を行うと変な挙動(ex 80 - 10 = 60)にはなりうる。
  - `SERIALIZABLE` では、`SELECT` は `SELECT FOR SHARE` で実行されるため、他のトランザクションに行の読み取りは許しても、UPDATE や DELETE は許可されない(=ブロックされる)
    - デッドロックに注意して実装しなければならない
    - またタイムアウト機能があるため、この分離レベルのトランザクションを実行する際はリトライ機能を実装しなければならない

| MySQL            | Dirty read | Non-repeatable read | Phantom read | Serialization anomaly |
| ---------------- | ---------- | ------------------- | ------------ | --------------------- |
| READ-UNCOMMITTED | x          | x                   | x            | x                     |
| READ-COMMITTED   | ✓          | x                   | x            | x                     |
| REPEATABLE-READ  | ✓          | ✓                   | ✓            | x                     |
| SERIALIZABLE     | ✓          | ✓                   | ✓            | ✓                     |

- Postgres 12
  - デフォの分離レベルは `read committed` (by `show transaction isolation level;`)
  - MySQL と異なり、トランザクション単位でしか分離レベルを変更できない
  - `read uncommitted` は実質的に存在せず、`read committed` と同様の挙動(dirty read は発生しない)
    - > PostgreSQL's Read Uncommitted mode behaves like Read Committed.
  - MySQL の REPEATABLE-READ は update で変な挙動をしていたが、Postgres ではエラーが発生する
    - > ERROR: could not serialize access due to concurrent update

| Postgres           | Dirty read | Non-repeatable read | Phantom read | Serialization anomaly |
| ------------------ | ---------- | ------------------- | ------------ | --------------------- |
| (read uncommitted) | ✓          | x                   | x            | x                     |
| read committed     | ✓          | x                   | x            | x                     |
| repeatable read    | ✓          | ✓                   | ✓            | x                     |
| SERIALIZABLE       | ✓          | ✓                   | ✓            | ✓                     |

## Report

- ストレージエンジン
  - テーブルにデータを書き込んだり読み込んだりする部分のこと
  - MySQL は色々選択肢はある(基本は InnoDB、CSV とかメモリとかも選択できる、MyISAM は旧標準で行ロックやトランザクションが無かった)
  - Postgres は 1 通りっぽい

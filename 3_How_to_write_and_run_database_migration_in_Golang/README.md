# How to write & run database migration in Golang

## Abst

- Go 言語で DB のマイグレーションを行えるツール
  - [golang-migrate/migrate: Database migrations. CLI and Golang library.](https://github.com/golang-migrate/migrate)
  - デファクトは無いっぽい。[github/gh-ost: GitHub's Online Schema Migrations for MySQL](https://github.com/github/gh-ost) とかも人気。

## Report

- [golang-migrate](https://github.com/golang-migrate/migrate) で CLI 操作
- Makefile の作法
  - そもそも Makefile のルールは、[作りたいファイル名]\(=**ターゲット**) と [そのファイルを作るためのコマンド] という組み合わせから成る。
  - したがってターゲットと同名のファイルが存在する場合、意図しない挙動になりがち
  - そこで、ターゲットを **擬似ターゲット** (正式名称は無いらしい。タスクと呼ばれることも。)として用いるときは、`PHONY` を用意し、タスク名を記述する。

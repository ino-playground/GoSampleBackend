# Generate CRUD Golang code from SQL | Compare db/sql, gorm, sqlx, sqlc

## Abst

- SQL のパッケージ 4 選
  - [database/sql](https://golang.org/pkg/database/sql/)
    - low-level 標準ライブラリ
    - メリット: 早い、簡単
    - デメリット: SQL フィールドを手動で変数にマッピングしなければならない
  - [Gorm](https://gorm.io/)
    - high-level オブジェクトリレーショナルマッピング(ORM)ライブラリ
    - メリット: CRUD がすでに実装されている
    - デメリット: ORM の文法を学ぶ必要あり、遅い(3~5 倍)
  - [Sqlx](https://github.com/jmoiron/sqlx)
    - メリット: 標準ライブラリと同等の速さ、結果を構造体のフィールドに自動的にスキャンしてくれる
    - デメリット: まだコードが冗長
  - [Sqlc](https://sqlc.dev/)
    - メリット: 標準ライブラリと同等の速さ、使いやすさ
- sqlc コネコネ
  - SQL の raw 文から Go コードを自動生成してくれる。codegen のタイミングで文法チェックもしてくれるから便利。
  - 「ORM 文法だと面倒だから SQL 生で書くか～？」みたいな無駄な悩みが無くなりそう。一方で汎用性の高い SQL 文を書くのはそれなりに頭を使うことになりそう。

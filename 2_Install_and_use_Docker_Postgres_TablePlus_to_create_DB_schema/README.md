# 2. Install & use Docker + Postgres + TablePlus to create DB schema

## Abst

- Install Docker
  - Image, Container, Port, TTY, Log の確認
- Database へ接続可能な GUI アプリケーションの紹介
  - [TablePlus | Modern, Native Tool for Database Management](https://tableplus.com/)
  - DBeaver みたいなものかな

## Report

- `docker logs <container_name_or_id>` でログが確認できるのは知らなかった

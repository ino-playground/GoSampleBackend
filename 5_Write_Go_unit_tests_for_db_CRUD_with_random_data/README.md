# 5. Write Go unit tests for db CRUD with random data

## Abst

- Go のテストコードを書く
  - 作法
    - テストファイル名の末尾は `_test`
    - テスト関数の先頭は `Test`、引数に `testing.T` を受け取る (T がテスト状態を管理、M がメインに渡されるもの、B がベンチマークなど、[いろいろな型が提供されている](https://pkg.go.dev/testing))
  - Go のユニットテストは、パッケージごとに別々に実行される。
  - [testify package](https://github.com/stretchr/testify) が便利
    - `assert` や `mock` を簡単に導入できる

## Report

- `go get` と `go install` の使い分け
  - get: `go.mod` 編集(依存性を記述する)のため
  - install: バイナリ(なにかしらのツールとか)のビルドとグローバルインストールのため
- 文字列連結
  - `+` 演算子は遅い
  - Go 1.10 で追加された、`strings.Builder` メソッドが最強

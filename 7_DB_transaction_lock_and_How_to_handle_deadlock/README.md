# DB transaction lock & How to handle deadlock

## Abst

- 生 SQL のロックを試してみよう
- 単純な方法ではデッドロックが発生する
  - `SELECT FROM accounts` が `INSERT INTO transfers` を実行する他のトランザクションからロックを取得する必要があるから
  - ∵ `accounts` テーブルと `transfers` テーブル間には**外部キー制約がある**
- ロック状態の監視
  - [Lock Monitoring - PostgreSQL wiki](https://wiki.postgresql.org/wiki/Lock_Monitoring)

## Report

- ポイント
  - ロック 2 種類
    - 共有ロック: 共有ロック同士は互いにブロックしない。ex) `SELECT ... LOCK IN SHARE MODE`
    - 占有ロック: 排他的。ex) `INSERT`, `UPDATE`, `DELETE`, `SELECT ... FOR UPDATE`
  - 強さ関係
    - 共有ロック中
      - :ok: 他のトランザクションからの共有ロックによる SELECT
      - :ng: 他のトランザクションからの UPDATE や SELECT ... FOR UPDATE 等
    - 占有ロック中
      - :ok: なし(?)
      - :ng: 他のトランザクションからの UPDATE や SELECT ... LOCK IN SHARE MODE / FOR UPDATE 等
    - デッドロックになる例
      - tx1: SELECT ... LOCK IN SHARE MODE
      - tx2: SELECT ... FOR UPDATE (<- 待ち発生)
      - tx1: SELECT ... FOR UPDATE (<- デッドロック発生)
  - 外部キー制約によるロック
    - :star: INSERST 時に外部キー制約される側(= 親)に共有ロックがかかる
- [ ] TODO: トランザクション・ロック・インデックスについて再勉強する
  - [ ] MySQL
    - [ ] インデックス
    - [ ] トランザクション
    - [ ] ロック
  - [ ] PostgresQL
    - [ ] インデックス
    - [ ] トランザクション
    - [ ] ロック

# Design DB schema and generate SQL code with dbdiagram.io

## Abst

- DB スキーマを設計する
  - [dbdiagraam.io](https://dbdiagram.io/home) を用いてグラフィカルに ER 図を確認
- Table, Type, references, Index 等基本的な DB 要素の確認

## Report

- dbdiagram.io は便利そうなので使っていきたい
